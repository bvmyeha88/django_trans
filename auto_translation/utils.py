import re

TAG_RE_KEEP     = re.compile(r'<[^(b>)>]+>')
TAG_RE_ALL      = re.compile(r'<[^>]+>')
TAG_RE_BOLD     = re.compile(r'<span style="font-weight: bold;">(.*)</span>')
TAG_RE_STRONG   = re.compile(r'<strong>(.*)</strong>')

def strip_html_tags(string, keep_simple_tags=False):
    s = string
    if s:
        if keep_simple_tags:
            s = TAG_RE_BOLD.sub('<b>\g<1></b>', s)
            s = TAG_RE_STRONG.sub('<b>\g<1></b>', s)
            s = TAG_RE_KEEP.sub('', s)
        else:
            s = TAG_RE_ALL.sub('', s)

        # TODO: Use regex
        s = s.replace('<br>', '')
    return s