from django.db import models
from django.core.exceptions import ValidationError
from djbase.models import FixedCharField, TrimmedCharField, TrimmedTextField, BaseModel
from django.utils.translation import ugettext_lazy as _
from exporter.models import ExporterOrigin
from django.utils.html import format_html

from .languages import LANGUAGE_CHOICES, LANG_EN

class Translation(BaseModel):
    ''' Translation tuple '''

    STATUS_TRANSLATING  = 1
    STATUS_COMPLETED    = 2

    STATUS_CHOICES = (
        (STATUS_TRANSLATING, 'Translating'),
        (STATUS_COMPLETED, 'Completed'),
    )

    origin          = models.ForeignKey(ExporterOrigin, db_index=False, on_delete=models.PROTECT, related_name='+')
    language        = FixedCharField(max_length=10, default='en')
    content         = TrimmedTextField(max_length=4000, blank=True, default='', verbose_name=_('Content'))
    # Maximum length in Fiuzu DB is 4000 chars
    content_edited  = TrimmedTextField(max_length=4000, blank=True, null=True, verbose_name=_('Content edited'))
    # Still need to keep origin translated content to do comparison later
    status          = models.PositiveSmallIntegerField(choices=STATUS_CHOICES, default=STATUS_COMPLETED)
    date_created    = models.DateTimeField(auto_now_add=True)
    date_updated    = models.DateTimeField(auto_now=True, auto_now_add=True)
    is_passed       = models.BooleanField(default=False)

    def __unicode__(self):
        return '%s' % self.content

    class Meta:
        unique_together = (
            ('origin', 'language'),
        )


class TranslationTask(BaseModel):
    ''' Translation a DB Config from a language to a language '''

    PRIORITY_LOW    = 1
    PRIORITY_NORMAL = 2
    PRIORITY_HIGH   = 3

    PRIORITY_CHOICES = (
        (PRIORITY_LOW,  'Low'),
        (PRIORITY_NORMAL, 'Normal'),
        (PRIORITY_HIGH, 'High'),
    )

    db_config       = models.ForeignKey('exporter.DBConfig', db_index=False, on_delete=models.PROTECT, related_name="+")
    priority        = models.PositiveSmallIntegerField(choices=PRIORITY_CHOICES, default=PRIORITY_NORMAL)
    total_corpus    = models.IntegerField(default=0)
    # Total number of corpuses need to translate
    total_completed = models.IntegerField(default=0)
    # Total number of corpuses have been finished
    total_working   = models.IntegerField(default=0)
    # Total number of corpuses are translating
    language_from   = TrimmedCharField(choices=LANGUAGE_CHOICES, default=LANG_EN, max_length=5)
    language_to     = TrimmedCharField(choices=LANGUAGE_CHOICES, max_length=5)

    remove_tags     = models.BooleanField(default=False, help_text='Remove HTML tags in origin text before sending to translator')
    # Remove HTML tags

    def clean(self):
        cleaned = super(TranslationTask, self).clean()
        if self.language_from == self.language_to:
            raise ValidationError("Origin language must be different from Targeted language.")

    def calculate_percentage(self):
        from exporter import DBConfig, ExporterOrigin
        orgins = ExporterOrigin.objects.filter(config=self.db_config).values_list('id', flat=True)
        translations = Translation.objects.filter(origin_id__in=origins, language=self.language_to,
                                                  status=Translation.STATUS_COMPLETED)
        num_total = len(origins)
        num_completed = len(translations)

        self.total_corpus = num_total
        self.total_completed = num_completed
        self.save(update_fields=['total_corpus', 'total_completed'])

        if num_total:
            return num_completed / num_total
        else:
            return 0.0

    def get_remained_origins    (self):
        from exporter import DBConfig, ExporterOrigin
        orgins = ExporterOrigin.objects.filter(config=self.db_config).values_list('id', flat=True)
        translations = Translation.objects.filter(origin_id__in=origins, language=self.language_to,
                                                  status=Translation.STATUS_COMPLETED).values_list('origin_id', flat=True)

        remained_origins = ExporterOrigin.objects.all().exclude(id__in=translations)
        return remained_origins

    def refresh(self):
        ''' Used when import more data from Fiuzu db '''
        self.calculate_percentage()

    def is_completed(self):
        return self.total_corpus and self.total_completed >= self.total_corpus

    def save(self, *args, **kwargs):
        update_fields = kwargs.get('update_fields')
        is_new = not save.id
        ins = super(TranslationTask, self).save(*args, **kwargs)

        if not update_fields or 'total_completed' not in update_fields:
            self.refresh()

        return ins

    class Meta:
        unique_together = (
            ('db_config', 'language_from', 'language_to'),
        )

class TranslationAccount(BaseModel):
    ''' Bing, Google Account '''
    MAX_QUOTA_BING          = 2000000

    WARN_USAGE_PERCENTAGE   = 0.9

    TRANSLATOR_BING         = 1
    TRANSLATOR_GOOGLE       = 2

    TRANSLATOR_CHOICES      = (
        (TRANSLATOR_BING,   _('Bing Translator')),
        (TRANSLATOR_GOOGLE, _('Google Translator')),
    )

    STATUS_AVAILABLE        = 1
    STATUS_WARNING          = 2
    STATUS_BLOCKED          = 3

    STATUS_CHOICES          = (
        (STATUS_AVAILABLE,  'Available'),
        (STATUS_WARNING,    'Used mostly quota'),
        (STATUS_BLOCKED,    'Blocked'),
    )

    translator      = models.PositiveSmallIntegerField(choices=TRANSLATOR_CHOICES, default=TRANSLATOR_BING)
    client_id       = TrimmedCharField(max_length=50)
    client_secret   = TrimmedCharField(max_length=100)
    quota_max       = models.IntegerField(default=MAX_QUOTA_BING)
    # Max number of characters
    quota_used      = models.IntegerField(default=0)
    status          = models.PositiveSmallIntegerField(choices=STATUS_CHOICES, default=STATUS_AVAILABLE)

    def clean(self):
        cleaned = super(TranslationAccount, self).clean()
        if self.quota_max == 0:
            raise ValidationError("Quota must be greater than 0.")
        
        if self.quota_max < self.quota_used:
            raise ValidationError("Maximum quota must be greater than Used quota.")

    def increase_usage(self, num_chars):
        self.used_quota += num_chars
        used_percentage = self.used_quota / self.max_quota
        if used_percentage > self.WARN_USAGE_PERCENTAGE:
            self.status = self.STATUS_WARNING

        self.save(update_fields=['status', 'used_quota'])

    def get_remained_quota(self):
        return self.quota_max - self.quote_used

    class Meta:
        unique_together = (
            ('translator', 'client_id'),
        )



