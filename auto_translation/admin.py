from django.contrib import admin
from .models import Translation, TranslationTask, TranslationAccount


class TranslationAdmin(admin.ModelAdmin):

    def _get_object_name(self, instance):
        if not instance.origin.object_name:
            return None
        return instance.origin.object_name
    _get_object_name.short_description = 'Object name'
    _get_object_name.allow_tags = True

    list_display = ('language', '_get_object_name', 'origin', 'content', 'content_edited', 'date_created', 'date_updated', 'is_passed')

    actions_on_top = True
    ordering = ['origin__object_id']
    search_fields = ['origin__object_id', 'language', 'content']
    sortable = 'origin__object_id'

    class Meta:
        model = Translation


class TranslationTaskAdmin(admin.ModelAdmin):
    list_display = ('db_config', 'language_from', 'language_to', 'total_corpus', 'total_working',
                    'total_completed', '_percentage', '_is_completed')

    actions = ['tasks_refresh']

    actions_on_top = True
    ordering = ['db_config']

    def tasks_refresh(self, request, queryset):
        for obj in queryset:
            obj.refresh()
    tasks_refresh.short_description = "Refresh tasks selected"

    def _percentage(self, ins):
        if ins.total_corpus:
            percentage = (float(ins.total_completed) / ins.total_corpus) * 100.0
        else:
            percentage = 0.0
        return percentage
    _percentage.short_description = 'Percentage (%)'

    def _is_completed(self, ins):
        return ins.is_completed()

    _is_completed.short_description = 'Completed'
    _is_completed.boolean = True

    class Meta:
        model = TranslationTask


class TranslationAccountAdmin(admin.ModelAdmin):
    list_display = ('translator', 'client_id', 'client_secret', 'status', 'quota_max', 'quota_used')

    actions_on_top = True
    ordering = ['translator', 'status']
    # search_fields = []

    class Meta:
        model = TranslationAccount

# Register Admin
admin.site.register(Translation, TranslationAdmin)
admin.site.register(TranslationTask, TranslationTaskAdmin)
admin.site.register(TranslationAccount, TranslationAccountAdmin)