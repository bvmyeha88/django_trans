LANG_EN = 'en'
LANG_TH = 'th'
LANG_MY = 'ms'
LANG_VI = 'vi'

LANGUAGE_CHOICES = (
    (LANG_EN,       'English'),
    (LANG_TH,       'Thais'),
    (LANG_MY,       'Malaysian'),
    (LANG_VI,       'Vietnamese'),
)