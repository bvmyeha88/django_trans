import json
from datetime import timedelta

from django.utils import timezone
from django.db.models import F, Q

from exporter.models import ExporterOrigin
from auto_translation.utils import strip_html_tags

from .translator import BingTranslator
from .models import Translation, TranslationTask, TranslationAccount


class TranslationNavigator(object):
    BULK_PACK_SIZE = 20000

    stop_at = 0
    # Stopable after translated number of chars

    can_stop = False
    # Stopable

    total_translated = 0
    # Number of chars tranlated

    def get_list_tasks(self):
        ''' Get list tasks need to do '''
        return TranslationTask.objects.all() \
                .exclude(total_completed=F('total_corpus')) \
                .order_by('priority')

    def get_bulk_pack(self, origins_list, remove_tags=False):
        ''' Divide all tasks into many bulk '''
        bulk = []
        list_bulk = []
        current_len = 0
        count = 0
        for origin in origins_list:
            text = origin.content if not remove_tags else strip_html_tags(origin.content)
            text_len = len(text)

            current_len += text_len
            self.total_translated += text_len

            # Update stopable status
            if self.stop_at and self.total_translated >= self.stop_at:
                self.can_stop = True

            # Push origin to bulk
            if current_len <= self.BULK_PACK_SIZE:
                bulk.append(origin)
                count += 1
            else:
                list_bulk.append(bulk)
                count = 0
                bulk = []
                current_len = len(origin.content)
                bulk.append(origin)
        # Still have data in bulk after iterating all origins.
        # This happens in case the whole bulk size is smaller 
        # than max bulk size.
        if bulk:
            list_bulk.append(bulk)

        return list_bulk

    def pick_an_account(self):
        ''' Select an available Account who still have quota '''
        try:
            account = TranslationAccount.objects.get(status=TranslationAccount.STATUS_AVAILABLE)
        except TranslationAccount.DoesNotExist:
            account = None

        return account

    def assign_to_account(self, task, bulk, account):
        ''' Let an account do translation '''
        usage = 0
        if account.translator == TranslationAccount.TRANSLATOR_BING:
            translator = BingTranslator(account.client_id, account.client_secret)
            for obj in bulk:
                text = obj.content if not task.remove_tags else strip_html_tags(obj.content)
                text_len = len(text)

                translated = translator.translate(text, task.language_from, task.language_to)
                print '- Tranlating obj %s from %s to %s ...' % (obj.pk, task.language_from, task.language_to)
                translation = Translation(origin=obj, language=task.language_to, content=translated)
                translation.save()
                usage += text_len

            # Update account usage
            account.quota_used += usage
            account.save(update_fields=['quota_used'])

    def do_translate(self, stop_at=0):
        ''' Main function. This assigns task to accounts to do translation 
        @param stop_at: let the translation top after translating a number of chars
        '''
        list_tasks = self.get_list_tasks()
        self.stop_at = stop_at

        for task in list_tasks:
            if self.can_stop:
                print 'Stop after translated %s chars.' % stop_at
                break

            task.refresh()
            if task.is_completed():
                continue
            remained_origins = task.get_remained_origins()
            print 'Working with %s origins ... ' % len(remained_origins)
            # Divide the total work into many bulks
            bulk_pack = self.get_bulk_pack(remained_origins, remove_tags=task.remove_tags)
            for bulk in bulk_pack:
                # Pick an available Account, who still has quota
                account = self.pick_an_account()
                if account:
                    # Let him do the translation
                    self.assign_to_account(task, bulk, account)
                    # Update usage and completion percentage
                    task.refresh()

        print 'Total translated: %s chars' % self.total_translated
        print 'Done'

