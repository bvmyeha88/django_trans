CREATE TABLE `exporter_dbconfig` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `database` varchar(50) NOT NULL,
    `table_name` varchar(50) NOT NULL,
    `field` varchar(50) NOT NULL,
    `language_code` char(10) NOT NULL,
    `main_key`  char(20)  NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `exporter_exporterorigin` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `config_id` integer NOT NULL,
    `language_code` char(10) NOT NULL,
    `content` varchar(3000),
    `object_id` char(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `exporter_exporterorigin` ADD CONSTRAINT `config_id_refs_id_7db83899` FOREIGN KEY (`config_id`) REFERENCES `exporter_dbconfig` (`id`);

CREATE TABLE `auto_translation_translation` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `origin_id` integer NOT NULL,
    `language_code` char(10) NOT NULL,
    `content` varchar(3000)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `auto_translation_translation` ADD CONSTRAINT `origin_id_refs_id_7db84500` FOREIGN KEY (`origin_id`) REFERENCES `exporter_exporterorigin` (`id`);

CREATE TABLE `auto_translation_translationtask` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `db_config_id` integer NOT NULL,
    `total_corpus` integer NOT NULL,
    `total_completed` integer NOT NULL,
    `language_from` smallint UNSIGNED NOT NULL,
    `language_to` smallint UNSIGNED NOT NULL
)
;
ALTER TABLE `auto_translation_translationtask` ADD CONSTRAINT `db_config_id_refs_id_a8f446af` FOREIGN KEY (`db_config_id`) REFERENCES `exporter_dbconfig` (`id`);
CREATE TABLE `auto_translation_translationaccount` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `translator` smallint UNSIGNED NOT NULL,
    `client_id` varchar(50) NOT NULL,
    `client_secret` varchar(100) NOT NULL,
    `quota_max` integer NOT NULL,
    `quota_used` integer NOT NULL,
    `status` smallint UNSIGNED NOT NULL
)
;

ALTER TABLE `auto_translation_translationtask` ADD COLUMN `priority` smallint UNSIGNED NOT NULL;
ALTER TABLE `auto_translation_translationtask` MODIFY `language_from` varchar(5) NOT NULL;
ALTER TABLE `auto_translation_translationtask` MODIFY `language_to` varchar(5) NOT NULL;
ALTER TABLE `auto_translation_translationtask` ADD COLUMN `remove_tags` bool NOT NULL;

ALTER TABLE `auto_translation_translation` ADD COLUMN `status` smallint UNSIGNED NOT NULL;
ALTER TABLE `auto_translation_translation` ADD COLUMN `date_created` datetime NOT NULL;
ALTER TABLE `auto_translation_translation` ADD COLUMN `date_updated` datetime NOT NULL;

UPDATE `auto_translation_translation` SET status = 2, date_created=NOW(), date_updated=NOW() WHERE 1=1;

ALTER TABLE `exporter_dbconfig` ADD COLUMN `main_name` varchar(20);
ALTER TABLE `exporter_exporterorigin` ADD COLUMN `object_name` char(100);

ALTER TABLE `auto_translation_translation` ADD COLUMN `is_passed` bool NOT NULL;
ALTER TABLE `auto_translation_translation` ADD COLUMN `content_edited` longtext;

ALTER TABLE `auto_translation_translationtask` ADD COLUMN `total_working` integer NOT NULL;
ALTER TABLE `auto_translation_translationtask` ADD COLUMN `remove_tags` bool NOT NULL;


-- Unique --
# TRUNCATE TABLE `auto_translation_translation`;
# DELETE FROM exporter_exporterorigin WHERE 1=1;

ALTER TABLE `auto_translation_translationaccount` ADD UNIQUE `auto_translation_translationaccount_unique_account` (`translator`, `client_id`);
ALTER TABLE `auto_translation_translationtask` ADD UNIQUE `auto_translation_translationtask_unique_task` (`db_config_id`, `language_from`, `language_to`);
ALTER TABLE `auto_translation_translation` ADD  UNIQUE `auto_translation_translation_unique_translation` (`origin_id`, `language`);

ALTER TABLE `exporter_dbconfig` ADD UNIQUE `exporter_dbconfig_unique_config` (`database`, `table_name`, `field`);
ALTER TABLE `exporter_exporterorigin` ADD UNIQUE `exporter_exporterorigin_unique_origin` (`config_id`, `object_id`);

ALTER TABLE `exporter_exporterorigin` MODIFY `object_name` char(200);

