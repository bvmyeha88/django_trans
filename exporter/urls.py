from django.conf.urls import patterns, include, url

from . import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'auto_translation.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^config/(?P<config_id>\d+)$', views.exporter_data, name="exporter_data"),
)
