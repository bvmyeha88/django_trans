from django.db import models
from djbase.models import FixedCharField, TrimmedCharField, TrimmedTextField, BaseModel
from django.utils.translation import ugettext_lazy as _


class DbConfig(BaseModel):

    database        = TrimmedCharField(max_length=50, verbose_name=_("Database name"))
    table_name      = TrimmedCharField(max_length=50, verbose_name=_("Table name"))
    field           = TrimmedCharField(max_length=50, verbose_name=_("Field name"))
    language        = FixedCharField(max_length=10, default='en')
    main_key        = TrimmedCharField(max_length=20, verbose_name=_("Main key"))
    main_name       = TrimmedCharField(max_length=20, verbose_name=_("Main name key"), null=True, blank=True)

    def __unicode__(self):
        return '%s.%s.[%s]' % (self.database, self.table_name, self.field)

    class Meta:
        ordering = ['database', 'table_name', 'field']
        unique_together = (
            ("database", "table_name", "field"),
        )


class ExporterOrigin(BaseModel):

    config          = models.ForeignKey(DbConfig, db_index=False, on_delete=models.PROTECT, related_name='+')
    language        = FixedCharField(max_length=10, default='en')
    content         = TrimmedTextField(max_length=3000, blank=True, default='', verbose_name=_('content'))
    object_id       = models.PositiveIntegerField(editable=False)
    object_name     = FixedCharField(max_length=200, null=True, blank=True)

    def __unicode__(self):
        return '%s' % self.content

    class Meta:
        unique_together = (
            ("config", "object_id"),
        )