from django.core.urlresolvers import reverse
from django.contrib import admin, messages

from exporter.models import DbConfig, ExporterOrigin

class DbconfigAdmin(admin.ModelAdmin):

    def _edit_url(self, instance):
        url= reverse("exporter:exporter_data", args=(instance.pk,))
        html = '<div class="fetch-data"><a href="%s">Fetch</a></div>' % url
        return html
    _edit_url.short_description = 'Edit'
    _edit_url.allow_tags = True

    list_display = ('database', 'table_name', 'field', 'language', 'main_key', 'main_name', '_origins', '_edit_url')
    list_filter = ['database', 'table_name']

    ordering = ['database']

    def _origins(self, ins):
        count = ExporterOrigin.objects.filter(config=ins).count()
        return count
    _origins.short_description = '# of origins'

    class Meta:
        unique_together = (
            ('database', 'table_name', 'field')
        )
        model = DbConfig

class ExporterOriginAdmin(admin.ModelAdmin):
    list_display = ('object_id', 'config', 'language', 'object_name', 'content')

    actions_on_top = True
    ordering = ['config__table_name']
    search_fields = ['object_id', 'config__table_name', 'config__field', 'language']
    list_filter = ['config', 'object_name']
    sortable = 'object_id'

    class Meta:
        unique_together = (
            ('object_id', 'config')
        )
        model = ExporterOrigin

# Register Admin
admin.site.register(DbConfig, DbconfigAdmin)
admin.site.register(ExporterOrigin, ExporterOriginAdmin)