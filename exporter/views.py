import json
import traceback

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.messages import error
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.http import Http404, HttpResponse

from django.db import connections
from .models import DbConfig, ExporterOrigin


def dictfetchall(cursor):
    "Returns all rows from a cursor as a dict"
    desc = cursor.description
    rows = cursor.fetchall()
    return [
        dict(zip([col[0] for col in desc], row))
        for row in rows
    ]

@login_required
def exporter_data(request, config_id):

    try:
        config = DbConfig.objects.get(pk=config_id)
    except DbConfig.DoesNotExist:
        raise Http404

    # Create query
    if config.main_name:
        query = "SELECT `%s`, `%s`, `%s` FROM `%s` WHERE `%s` != '' AND `%s` IS NOT NULL" % \
                (config.main_key, config.field, config.main_name, config.table_name, config.field, config.field)
    else:
        query = "SELECT `%s`, `%s` FROM `%s` WHERE `%s` != '' AND `%s` IS NOT NULL" % \
                (config.main_key, config.field, config.table_name, config.field, config.field)

    # TODO: Should minimize function in try to catch targeted exceptions
    try:
        cursor = connections['fiuzu'].cursor()
        cursor.execute(query)
        dict_res = dictfetchall(cursor)
    except Exception as e:
        traceback.print_exc()
        messages.info(request, 'ERROR: %s' % str(e))
    else:
        num_insert = 0
        num_error = 0

        objs = []
        for res in dict_res:
            origin = ExporterOrigin(config=config,
                                    language=config.language,
                                    content=res[config.field],
                                    object_id=res[config.main_key],
                                    object_name=res.get(config.main_name))

            # Check validation and unique
            try:
                origin.full_clean()
            except Exception as e:
                print e
                num_error += 1
            else:
                objs.append(origin)

        if objs:
            msg = ExporterOrigin.objects.bulk_create(objs)
            msg.save()
            num_insert = len(objs)

        messages.info(request, "Fetched done with %s row(s). Error: %s row(s)" % (num_insert, num_error) )
    finally:
        if cursor:
            cursor.close()
        return redirect('/admin/exporter/exporterorigin/?config__id__exact=%s' % config.pk, permanent=False)
